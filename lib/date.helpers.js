const fecha = require('fecha')

/**
 * Return the total days in a month for a specific year
 * 
 * @param {number} month - 1 for January
 * @param {number} year
 * @return {number} amount of days in the month
 */
module.exports.daysInMonth = function(month, year) {
	return new Date(year, month, 0).getDate()
}

/**
 * Convert date object in a map with day, month and year
 * 
 * @param {Date} date - A date instance or today by default
 * @return {{ day: number, month: number, year: number }}
 */
module.exports.dateToObj = module.exports.parseDate = function(date = new Date()){
	return { day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear() }
}

module.exports.toDate = function({ day, month, year }){
	return new Date(year, month - 1, day)
}

const backendFormat = 'YYYY-MM-DD'

/**
 * @param {string} serverDateStr
 * @param {string} formatPattern
 * @return {string}
 * 
 * Example: 
 * formatServerToUI('2012-02-01', 'DD/MM/YYYY')
 * // => '01/02/2012
 */
module.exports.fromServerToUI = function(serverDateStr, formatPattern){
	const date = fecha.parse(serverDateStr, backendFormat)
	return fecha.format(date, formatPattern)
}

module.exports.parseServerDate = function(serverDateStr){
	return fecha.parse(serverDateStr, backendFormat)
}


module.exports.format = (date, format) => fecha.format(date, format)

/**
 * 
 * @typedef {array} dayTuple
 * @property {number} 0 - Year
 * @property {number} 1 - Month [ 1..12 ]
 * @property {number} 2 - Day
 * 
 */
module.exports.dateToTuple = function(date){
	return [ date.getFullYear(), date.getMonth() + 1, date.getDate() ]
}

module.exports.tupleToDate = function([ year, month, day ]) {
	return new Date(year, month - 1, day)
}

/**
 * @param {Date} date 
 * @returns {number} the number of day in the week. Monday: 1, .... Saturday: 6, Sunday: 7
 */
module.exports.dayOfWeek = (date) => date.getDay() || 7


/**
 * @param {string} uiDate
 * @param {string} uiFormatPattern
 * @return {string}
 * 
 * Example: 
 * fromUItoServer('21/07/2017', 'DD/MM/YYYY')
 * // => '2017-07-21'
 */
module.exports.fromUItoServer = function(uiDate, uiFormatPattern){
	const date = fecha.parse(uiDate, uiFormatPattern)
	return fecha.format(date, backendFormat)
}


module.exports.obtainDateSeparator = function(format){
	const result = /^\w{2,4}(.{1})\w{2,4}.{1}\w{2,4}$/g.exec(format)
	return result ? result[1] : null
}

const MS_PER_DAY = 1000 * 60 * 60 * 24;

module.exports.dateDiffInDays = function(a, b){
	const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate())
	const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate())
	return Math.floor((utc2 - utc1) / MS_PER_DAY)
}

module.exports.isWithinRange = require('date-fns/is_within_range')


module.exports.extractTime = function(datetimeStr){
	//"2018-01-04T07:05:00"
	const pattern = /T(\d{2}:\d{2}):\d{2}/g
	const result = pattern.exec(datetimeStr)
	return result[1]
}