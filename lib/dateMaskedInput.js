const m = require('mithril')

// https://github.com/text-mask/text-mask
const textMaskCore = require('text-mask-core')
const createAutoCorrectedDatePipe = require('text-mask-addons/dist/createAutoCorrectedDatePipe').default
const { obtainDateSeparator } = require('./date.helpers')

const map = require('ramda/src/map')
const intersperse = require('ramda/src/intersperse')
const split = require('ramda/src/split')
const pipe = require('ramda/src/pipe')
const flatten = require('ramda/src/flatten')

function noop(){}

const maskMap = {
	'd': [ /\d/, /\d/ ],
	'm': [ /\d/, /\d/ ],
	'y': [ /\d/, /\d/, /\d/, /\d/ ]
}


module.exports = (vnode) => {

	let textMask;

	const attrs = vnode.attrs
	const originalOnInput = attrs.oninput || noop
	const format = (attrs.format || 'DD/MM/YYYY').toLowerCase()
	// Extract separator. Example:
	// dd/mm/YYYY -> /
	// YYYY-MM-DD -> -
	const separator = obtainDateSeparator(format)


	const mask = pipe(
		split(separator),
		map(value => value.charAt(0)),
		map(char => maskMap[char]),
		intersperse(separator), // mask[0], separator, mask[1], separator, mask[2]
		flatten
	)(format)


	attrs.oncreate = (vnode) => {
		textMask = textMaskCore.createTextMaskInputElement({
			inputElement: vnode.dom,
			mask: mask,
			guide: false,
			showMask: false,
			keepCharPositions: true,
			pipe: createAutoCorrectedDatePipe(format)
		})
	}	

	attrs.oninput = (e) => {
		textMask.update(e.target.value)
		attrs.value = e.target.value
		originalOnInput(e)
	}

	return {
		view: () => m('input[type=text]', attrs)
	}
}
