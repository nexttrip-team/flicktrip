/* eslint no-constant-condition: "off" */

import nanoid from 'nanoid'
import trackWithConsole from './trackWithConsole'
import Rollbar from 'rollbar'

export default function ({ env, rollbarAccessToken, commitVersion }) {
	let logger;

	if( env === 'production' || env === 'stage' ){
		logger = new Rollbar({
			accessToken: rollbarAccessToken,
			captureUncaught: true,
			autoInstrument: true,
			enabled: true,
			payload: {
				environment: env,
				client: {
					javascript: {
						source_map_enabled: true,
						code_version: commitVersion,
						guess_uncaught_frames: true
					}
				}
			}
		})
	} else {
		logger = trackWithConsole
	}


	return {
		setUser({ name, email }) {
			logger.configure({
				payload: {
					person: {
						id: email,
						username: name,
						email
					}
				}
			})
			logger.info('new user')
		},

		setApp(customerId, dealId, productId) {
			logger.configure({
				payload: {
					environment: env,
					context: `${customerId}|${dealId}|${productId || ''}`,
					person: { sessionId: nanoid() }
					//code_version:
				}
			})
		},

		log:      (...args) => logger.log(...args),
		info:     (...args) => logger.info(...args),
		warning:  (...args) => logger.warning(...args),
		error:    (...args) => logger.error(...args),
		critical: (...args) => logger.critical(...args)
	}

}


