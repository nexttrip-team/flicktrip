const m = require('mithril')

module.exports =  {
	view: ({ attrs, children }) => {
		const { errors = [] } = attrs
		return errors.length > 0
			? m('.form-error', [
					children,
					m('span.form-error__msg', errors)
				])
			: children
	}
}