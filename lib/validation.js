import validate from 'validate.js'
import mapObjIndexed from 'ramda/src/mapObjIndexed'
import fecha from 'fecha'

validate.validators.stringDate = require('./date.validator')


validate.extend(validate.validators.datetime, {

	// Date to unix timestamp
	parse: function(value, options) {
		if( ! value ) return NaN
		if( value instanceof Date ) return value.getTime()
		const parsed = fecha.parse(value, options.format)
		return parsed ? parsed.getTime() : NaN
  },

	//unix timestamp to date
	format: function(value, options) {
		return fecha.format(value, options.format)
	}

})

const mapErrors = mapObjIndexed((errors) => errors[0])

function noop(){}


export default {
	validate(contraints, model) {
		const error = validate(model, contraints)

		return {
			matchWith: ({ Success = noop, Failure = noop }) => (
				error ? Failure(mapErrors(error)) : Success(model)
			)
		}
	},

	// Receipt a key, value of validation messages
	addTranslations(translationMessages) {
		const tmessage = (tkey) => `^${translationMessages[tkey]}`

		Object.keys(translationMessages).forEach(tkey => {
			const [type, subtype] = tkey.split('.')
			if (!validate.validators[type]) {
				console.error('trying to set a message in an invalid validator (' + type + ')')
				return
			}

			if (subtype) {
				if (!validate.validators[type].options) { validate.validators[type].options = {} }
				validate.validators[type].options[subtype] = tmessage(tkey)
			} else {
				validate.validators[type].message = tmessage(type)
			}
		})

	}


}


