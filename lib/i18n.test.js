import { test } from 'tape'
import i18n from './i18n'

test('i18n', test => {
	const t = i18n({
		'label': 'label 1',
		'page_1': {
			label: 'page_1 label 1'
		},
		'page_2': {
			label: 'page_2 label 1'
		}
	})

	test.equal(t('label'), 'label 1')
	test.equal(t('page_1', 'label'), 'page_1 label 1')

	const t2 = t.of('page_1')
	test.equal(t2('label'), 'page_1 label 1')
	test.equal(t2.root('label'), 'label 1')

	const t3 = t.of('page_2')
	test.equal(t3('label'), 'page_2 label 1')
	test.equal(t3.root('label'), 'label 1')

	test.end()
})