/**
 * Mock the Rollbar client
 */
export default {
	configure() { },
	log(...args) { console.log(...args) },
	info(...args) { console.info(...args) },
	warning(...args) { console.warn(...args) },
	error(...args) { console.error(...args) },
	critical(...args) { console.error(...args) }
}