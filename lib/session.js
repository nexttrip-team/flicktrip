
// For test purposes
const sessionStorage = window.sessionStorage || require('sessionstorage')

const secureParse = maybeJSON => {
	try {
		return JSON.parse(maybeJSON)
	} catch( error ) {
		return maybeJSON
	}
}

export default function(prefix) {

	const mkey = key => `${prefix}:${key}`

	function session(key, value) {
		if (key && value === undefined) return secureParse(sessionStorage.getItem(mkey(key)))
		if (key && !value) return sessionStorage.removeItem(mkey(key))
		return sessionStorage.setItem(mkey(key), JSON.stringify(value))
	}

	session.clear = () => {
		Object.keys(sessionStorage).forEach(key => {
			if( key.startsWith(prefix + ':') ){
				sessionStorage.removeItem(key)
			}
		})
	}

	session.all = () => {
		Object.keys(sessionStorage).reduce((s, key) => {
			if (key.startsWith('nextTrip')) {
				s[key] = sessionStorage[key]
			}
			return s
		}, {})
	}

	return session

}

