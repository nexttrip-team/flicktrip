//import m from 'mithril'
import assert from 'assert'

// see: https://github.com/StephanHoyer/translate.js
import translate from 'translate.js'

/**
 * Example of use:
 * 
 * const i18n = require('./i18n')
 * 
 * const t = i18n({
 * 	sayHello: 'Hello {user}!',
 * 	form: {
 * 		title: 'My form',
 * 		inputPlaceholder: 'type your user'
 * 	}
 * })
 * 
 * 
 * t('sayHello', { user: 'John' }) 
 * // => 'Hello John'
 * 
 * const t2 = t.of('form') // hold the key form
 * t2('title')  // use 'title' as subkey of 'form'
 * // => 'My form'
 * 
 * t2.root('sayHello', { user: 'John' }) // unlock the key, use the root as context
 * // => 'Hello John'
 * 
 * 
 */

export default (messages, lang) => {
	assert.ok(messages, 'Trying to load empty messages')
	const t = translate(messages)
	const months = (t.keys['monthNames'] || '').split(',')

	/**
	 * build a translator for the translations loaded, fixed to root of messages
	 * @param {string | null} key 
	 */
	function translator(key) {

		const translations = (...args) => {
			return key ? t(key, ...args) : t(...args)
		}

		translations.lang = lang

		/**
		 * Return the key: values map of the translations
		 */
		translations.keys = () => {
			return key ? t.keys[key] : t.keys
		}

		/**
		 * Use in case of the translation have HTML code (to avoid the code escape).
		 * Use with care ( ensure there's no user-generated malicious code in the HTML string )
		 * @see https://mithril.js.org/trust.html
		 */
		//translations.trust = (...args) => m.trust(translations(...args))


		translations.dateFormat = () => t.keys['dateFormat']

		translations.monthName = number => months[number]
		translations.monthNames = () => months

		/**
		 * Translation function without use the key
		 */
		translations.root = t

		/**
		 * new translator, same translation different key fixed
		 * @param {string} key
		 */
		translations.of = translator

		return translations
	}

	return translator()
}

