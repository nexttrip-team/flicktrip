import merge from 'ramda/src/merge'
import range from 'ramda/src/range'
import all from 'ramda/src/all'
import map from 'ramda/src/map'
import { daysInMonth, parseDate } from './date.helpers'

const allNumbers = all(maybeNumber => ! isNaN(maybeNumber))

const validYear  = value => value >= 1900 && value <= parseDate().year
const validMonth = value => value >= 1 &&  value <= 12
const validDay   = (year, month, day) => day >= 1 && day <= daysInMonth(month, year)

const validDate = ({ day, month, year }) => {
	return validYear(year) && validMonth(month) && validDay(year, month, day)
}

const formatItems = { d: 'day', m: 'month', y: 'year' }

const mapToObj = (format, items) => {
	const result = {}
	range(0, 3).forEach(position => {
		const type = formatItems[format.charAt(position)] // convert every char of format into names. 'd' -> 'day'
		result[type] = items[position]
	})
	return result
}


export default function validator(value, options){
	if( ! value ) return null

	const config = merge({
		separator: '/',
		format: 'dmy'
	}, options || {})

	const listOfStr = value.split(config.separator)

	const isValid = allNumbers(listOfStr) 
		&& validDate(
			mapToObj(config.format, map(value => parseInt(value), listOfStr))
		)

	return isValid ? null : config.message || validator.message
}

validator.message = 'invalid date'

