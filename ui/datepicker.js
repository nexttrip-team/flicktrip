import Flatpickr from 'flatpickr'
import { obtainDateSeparator } from '../lib/date.helpers'
import merge from 'ramda/src/merge'

import 'flatpickr/dist/flatpickr.min.css'

// https://chmln.github.io/flatpickr/formatting/
const formats = {
	d: 'd',
	m: 'm',
	y: 'Y'
}

const mapDateFormat = (dateFormat = 'DD/MM/YYYY') => {
	const separator = obtainDateSeparator(dateFormat)
	return dateFormat
		.split(separator)
		.map(value => value.charAt(0).toLowerCase())
		.map(char => formats[char])
		.join(separator)
}


/**
 * @param {DOM Element} el
 * @param {Object} more
 * @param {string} more.dateFormat - Example: 'DD/MM/YYYY'
 * @param {boolean} more.noCalendar
 * And all the options present in https://chmln.github.io/flatpickr/options/
 * 
 * @example 
 * 
 *	m('input[type=text].form-control', {
 *		name: 'date',
 *		placeholder: t('dateLabel'), 
 *		value: state.form.date,
 *		oncreate: vnode => { 
 *			datepicker(vnode.dom, {
 *				dateFormat: t.dateFormat(),
 *				defaultDate: state.form.date,
 *				minDate: state.minDate,
 *				onChange: (_, value) => {
 *					actions.setValue('date')(value)
 *				}
 *			}) 
 *		}
 *	})
 * 
 * JSX example:
 * 
 * <input type="text" name="date" 
 * 	placeholder={ t('dateLabel') }
 * 	value={ state.form.date }
 * 	oncreate={ vnode => {
 * 		datepicker(vnode.dom, {
 *			dateFormat: t.dateFormat(),
 *			defaultDate: state.form.date,
 *			minDate: state.minDate,
 *			onChange: (_, value) => {
 *				actions.setValue('date')(value)
 *			}
 *		})
 * 	}}
 * />
 * 


 */
export function datepicker(el, more){
	const config = more.noCalendar ? more : merge(more, {
		dateFormat: mapDateFormat(more.dateFormat, more.noCalendar)
	})

	return Flatpickr(el, config)
}

export function localize(t){

	const data = {
		weekdays: {
			shorthand: t('weekdaysShorthand').split(', '),
			longhand: t('weekdaysLonghand').split(', ')
		},
		months: {
			shorthand: t('monthsShorthand').split(', '),
			longhand: t('monthsLonghand').split(', '),
		},
		ordinal: () => t('ordinal')
	}

	Flatpickr.localize(data)
}
