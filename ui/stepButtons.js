import m from 'mithril'
import { maybeVisible, Row, Col, Icon } from './tags'
import assert from 'assert'
/*

<StepButtons
	visible={true}
	next="" 
	back="" 
	onclickNext={}
	onclickBack={}
/>

*/
export const StepButtons = maybeVisible(attrs => {

	assert( attrs.next || attrs.back, 'Must be set back or next value' )

	return (
		<Row class="py-4 f4 mb-md-4">
			<Col of="6" class="text-left ">
				<BackBtn visible={ attrs.back } 
					onclick={ attrs.onclickBack }
					label={ attrs.back } />
			</Col>
			<Col of="6" class="text-right">
				<NextBtn visible={ attrs.next }
					onclick={ attrs.onclickNext }
					label={ attrs.next } />
			</Col>
		</Row>
	)
})


function emit(clickFn) {
	return function name(e) {
		if (!clickFn) return true
		e.preventDefault()
		return clickFn()
	}
}

const stepButton = attrs => (
	<button type="submit" class="button" onclick={ emit(attrs.onclick) }>
		{attrs.label}
		<Icon name={attrs.icon} class="v-align-bottom" />
	</button>
)


export const BackBtn = maybeVisible(attrs => stepButton(
	Object.assign({}, attrs, { icon: 'chevron-left' })
))

export const NextBtn = maybeVisible(attrs => stepButton(
	Object.assign({}, attrs, { icon: 'chevron-right' })
))
