import evolve from 'ramda/src/evolve'
import update from 'ramda/src/update'
import append from 'ramda/src/append'
import remove from 'ramda/src/remove'
import validator from '../../lib/validation'
import { fromUItoServer, fromServerToUI, parseDate, toDate } from '../../lib/date.helpers'


// Validation contraints
const buildContraints = t => ({
	firstName: { presence: true }, 
	lastName: { presence: true },
	genre: { presence: true },
	email: { presence: true , email: true },
	documentId: { presence: true },
	birthDate: {
		presence: true,
		datetime: {
			//dateOnly: true,
			format: t.dateFormat(),
			latest: new Date(),
			message: `^${t('invalidDate')}`,
			earliest: date130yearsAgo(),
			tooEarly: `^${t('birthDateTooEarly')}`,
			tooLate: `^${t('birthDateTooLate')}`
		}
	}
})

function date130yearsAgo(){
	let date = parseDate(new Date())
	date.year = date.year - 130
	return toDate(date)
}


/**
 * @typedef {Object} Form
 * @prop {string} firstName
 * @prop {string} lastName
 * @prop {string} genre - MASCULINO | FEMENINO
 * @prop {string} documentId
 * @prop {string} birthDate - Format based on lang (t.dateFormat)
 * @prop {string} email
 */

 /**
	* @typedef {Object} Passenger
	* @prop {string} firstName
	* @prop {string} lastName
	* @prop {string} genre - MASCULINO | FEMENINO
	* @prop {string} documentId
	* @prop {string} birthDate - Format to server (YYYY-MM-DD)
	* @prop {string} email
	*/


const LIST_VIEW = 'list'
const FORM_VIEW = 'form'

module.exports = ({ setPassengers, passengers, requiredNumberOfPax, next, t }) => {

	const pax = passengers || []

	const state = {
		passengers: pax,
		current: {},
		editingIndex: null,
		requiredNumberOfPax,
		errors: {},
		view: pax.length > 0 ? LIST_VIEW : FORM_VIEW
	}

	const commit = () => {
		setPassengers(state.passengers)
	}

	const contraints = buildContraints(t)

	const cleanEditingContext = () => { 
		state.editingIndex = null
		state.current = {}
	}
	
	// modelToForm :: model -> form
	const modelToForm = evolve({
		birthDate: value => fromServerToUI(value, t.dateFormat())
	})

	// formToModel :: form -> model
	const formToModel = evolve({
		birthDate: value => fromUItoServer(value, t.dateFormat()),
		firstName: value => value.trim(),
		lastName: value => value.trim(),
	})

	const actions = {

		setField: field => value => { state.current[field] = value },

		savePassenger: () => {
			state.errors = {}
			validator.validate(contraints, state.current).matchWith({
				Failure: errors => { state.errors = errors },
				Success: form => {
					const add = state.editingIndex !== null ? update(state.editingIndex) : append
					state.passengers = add(formToModel(form), state.passengers)
					commit()
					cleanEditingContext()
					state.view = LIST_VIEW
				}
			})
		},

		cancelFormLoad: () => {
			cleanEditingContext()
			state.view = state.passengers.length > 0 ? LIST_VIEW : FORM_VIEW
		},

		edit: (passengerIndex) => {
			state.current = modelToForm(state.passengers[passengerIndex])
			state.editingIndex = passengerIndex
			state.view = FORM_VIEW
		},

		remove: (passengerIndex) => {
			state.passengers = remove(passengerIndex, 1, state.passengers)
			state.view = state.passengers.length > 0 ? LIST_VIEW : FORM_VIEW
			commit()
		},

		continue: () => {
			if( state.passengers.length !== state.requiredNumberOfPax ) {
				cleanEditingContext()
				state.view = FORM_VIEW
			} else {
				commit()
				next()
			}
			//if( state.passengers.length !== state.requiredNumberOfPax ) return
		}
	}

	return { state, actions, t }
}