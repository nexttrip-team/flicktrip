import m from 'mithril'

import { Row, Col, Icon, tag } from '../tags'
import { StepButtons } from '../stepButtons'
import './pax-list.css'

const Btn = tag(
	({ onclick, icon, label }) => (
		<a class="btn-sm d-inline-block" onclick={ onclick }>
			<Icon name={ icon }  class="d-inline v-align-bottom" />
			<span class="pl-2 d-none d-sm-inline text-uppercase fw-100 f7">{ label }</span>
		</a>
	)
)

const shouldAddMorePassengers = ({ requiredNumberOfPax, passengers }) => passengers.length < requiredNumberOfPax

const passengersLabel = (t, { passengers, requiredNumberOfPax }) => {
	const key = passengers.length > 1 ? 'nPaxOf' : 'onePaxOf'
	return t(key, { pax: passengers.length, total: requiredNumberOfPax })
}

const nextPassengerNumber = (state) => {
	return state.passengers.length + 1
}

const nextLabel = (state, t) => {
	if( shouldAddMorePassengers(state) ) {
		return t('addPassenger', { n: nextPassengerNumber(state) })
	}
	
	return t.root('nextStep')
}

export default ({ state, actions, t }) => (
	<div>
			<Row class="mt-4">
				<Col class="text-center">
					<div class="bg-color-2 px-3 py-2 place d-inline-block">
						<Icon name="users" class="mr-2 v-align-bottom" />
						{ passengersLabel(t, state) }
					</div>
				</Col>
			</Row>

			<Row class="my-4">
				<Col>
					{ state.passengers.map((p, index) => (
						<Row class="pax-list__row mx-1 pt-3 pb-2">
							<Col of="8" class="col-sm-6 text-truncate text-uppercase">
								{ `${ index + 1 }. ${p.firstName} ${p.lastName}` }
							</Col>
							<Col of="2" class="text-right col-sm-3">
								<Btn icon="edit-3" label={ t('edit') }
									onclick={ () => { actions.edit(index) } } />
							</Col>
							<Col of="2" class="text-right col-sm-3">
								<Btn icon="trash" label={ t('delete') }
									onclick={() => { actions.remove(index) }} />
							</Col>
						</Row>
					)) }
				</Col>
			</Row>


			<StepButtons visible={ true }
				next={ nextLabel(state, t) }
				onclickNext={ actions.continue }
			/>
	</div>
)
