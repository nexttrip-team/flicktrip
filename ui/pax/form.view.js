
import m from 'mithril'
import DateMaskedInput from '../../lib/dateMaskedInput'
import ValidationMsgs from '../../lib/validate.feedback'

import { Row, Col, Icon, Section } from '../tags'
import { NextBtn } from '../stepButtons'
import Select from '../select.js'

export default ({ state, actions, t }) => {
	const passenger = state.current || {}
	const passengerNumber = (state.editingIndex === null ? state.passengers.length : state.editingIndex) + 1

	function submitForm(e){
		e.preventDefault()
		actions.savePassenger(e.target)
	}

	return (
		<div>
			<div class="content">
				<Row class="mt-4">
					<Col class="text-center">
						<div class="bg-color-2 px-3 py-2 place d-inline-block text-uppercase">
							<Icon name="user" class="mr-2 v-align-bottom"/>
							{ t('passengerNumber', { number: passengerNumber, total: state.requiredNumberOfPax }) }
						</div>
					</Col>
				</Row>

				<form class="form" onsubmit={ submitForm } novalidate>
					<Row class="mt-5">
						<Col class="mb-3">
							<ValidationMsgs errors={state.errors.firstName }>
								<input type="text"
									name="firstName" placeholder={ t('firstName') }
									value={ passenger.firstName }
									oninput={ m.withAttr('value', actions.setField('firstName')) } />
							</ValidationMsgs>
						</Col>

						<Col class="mb-3">
							<ValidationMsgs errors={state.errors.lastName}>
								<input type="text"
									name="lastName" placeholder={t('lastName') }
									value={passenger.lastName}
									oninput={m.withAttr('value', actions.setField('lastName'))} />
							</ValidationMsgs>
						</Col>

						<Col class="mb-2">
							<ValidationMsgs errors={state.errors.genre}>
								<Select required
										name="genre" placeholder={ t('genre') } 
										value={ passenger.genre }
										onchange={ m.withAttr('value', actions.setField('genre')) } >
									<option value="" disabled selected>{ t('genre') }</option>
									<option value="FEMENINO">{ t('female') }</option>
									<option value="MASCULINO">{ t('male') }</option>
								</Select>
							</ValidationMsgs>
						</Col>

						<Col class="mb-3">
							<ValidationMsgs errors={state.errors.documentId}>
								<input type="text"
									name="documentId" placeholder={t('documentId')}
									value={passenger.documentId}
									oninput={m.withAttr('value', actions.setField('documentId'))} />
							</ValidationMsgs>
						</Col>

						<Col class="mb-3">
							<ValidationMsgs errors={ state.errors.birthDate }>
								<DateMaskedInput name="birthDate"
									placeholder={ t('birthdate') }
									value={ passenger.birthDate }
									oninput={ m.withAttr('value', actions.setField('birthDate')) } />
							</ValidationMsgs>
						</Col>

						<Col class="mb-3">
							<ValidationMsgs errors={state.errors.email}>
								<input type="email"
									name="email" placeholder={t('email')}
									value={passenger.email}
									oninput={m.withAttr('value', actions.setField('email'))} />
							</ValidationMsgs>
						</Col>

					</Row>

					<Row class="py-4 f4">
						<Col of="6" class="text-left text-uppercase">
							<Section visible={state.passengers.length > 0}>
								<a class="btn" onclick={actions.cancelFormLoad}>
									{ t('cancel') }
								</a>
							</Section>
						</Col>
						<Col of="6" class="text-right">
							<NextBtn visible label={ t('save') } />
						</Col>
					</Row>


				</form>

			</div>
		</div>
	)

}
