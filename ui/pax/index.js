import handler from './handler'
import form from './form.view'
import list from './list.view'

const views = {
	form, list
}

/**
 * 
 */
export default {
	oninit: vnode => {
		const { updatePax, pax, numberOfPax, next } = vnode.attrs
		//const { app, next } = vnode.attrs
		const t = vnode.attrs.t.of('personalInfo')

		//const { numberOfPax, pax } = app.passengersData()

		const n = handler({
			setPassengers: updatePax,
			passengers: pax,
			requiredNumberOfPax: numberOfPax,
			next,
			t
		})

		vnode.state = n

	},

	/**
	 * state.view -> form | list
	 */
	view: (vnode) => {
		const { state, actions, t } = vnode.state
		const view = views[state.view]
		return view({ state, actions, t })
	}
	
}
