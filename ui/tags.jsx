/** @jsx dom */

import feather from 'feather-icons'
import m from 'mithril'

export function tag(viewFn) {
	return {
		view(vnode) {
			return viewFn(vnode.attrs, vnode.children)
		}
	}
}

export function maybeVisible(viewFn) {
	return tag(function (attrs, children) {
		return attrs.visible ? viewFn(attrs, children) : ""
	})
}

export function Section(){ 
	return maybeVisible((attrs, children) => children)
}

const clone = obj => Object.assign({}, obj)

export const Row = tag((attrs, children) => {
	return m('div.row', attrs, children)
})

export const Col = tag((attrs, children) => (
	<div class={`col-${attrs.of || 12} ${attrs.class || ""}`}>{children}</div>
))


/**
 * 
 * Ejemplos de uso:
 * 
 * ```
 * <Icon name="more-vertical" />
 * ```
 * 
 * Donde "name" es el nómbre del ícono.  Ver la lista de íconos acá: https://feathericons.com/
 * 
 */
export const Icon = tag(
	({ name, ...more }) => m.trust(feather.icons[name].toSvg(more || {}))
)