import m from 'mithril'

import { Row, Col, Icon, maybeVisible } from '../tags'
import './index.css'

export default {

	view(vnode){

		const {
			depositPercentage = 0,
			totalPrice = 0,
			depositPrice = 0,
			options = [],
			t
		} = vnode.attrs

		return (
			<div>
				<Row class="mt-4">
					<Col class="text-center">
						<div class="bg-color-2 px-3 py-2 place d-inline-block">
							<span class="d-inline-block pr-1">{ t('title') }</span>
							<span class="d-inline-block open">USD { totalPrice.toLocaleString(t.lang) }</span>
						</div>
					</Col>
				</Row>
				<Row class="text-center">
					<Col class="my-4 fw-100 text-center">
						{ t('depositHint', { depositPercent: depositPercentage.toLocaleString(t.lang) }) }
						<strong class="price fw-600 open">USD { depositPrice.toLocaleString(t.lang) }</strong>
					</Col>
					<Col class="mb-3 fw-100 text-center">
						{ t('paymentSelectionTitle') }
					</Col>
				</Row>
				<Row>
				{
					options.map(option => (
						<Col class="text-center">
							<button type="button"
								class="button button-bordered"
								onclick={ option.action }>
								{ option.label }
							</button>
						</Col>
					))
				}
				</Row>

			</div>
		)
	}
}
