const m = require('mithril')
const spinner = require('../spinner')
const { Row, Col, tag, Section } = require('../tags')
const { StepButtons } = require('../stepButtons')
const Modal = require('../modal')
require('./style.css')


module.exports = vnode => {
	const { model, actions, t } = vnode.state

	function openModal(e) {
		e.preventDefault()
		actions.toggleTermAndConditions()
	}

	if( model.loading ) return spinner()
	
	return (
		<div>

			{ model.content.map(item => [ 
					<Label>{ item.label }</Label>,
					<Row class="mt-3 ">
						<Col class="text-center fw-100 ">
							{ item.content }
						</Col>
					</Row>
				]) 
			}
			
			<Row class="mt-5 mb-5">
				<Col class="text-center ">
					<label>

						<input type="checkbox" id="styled-checkbox-1" class="mr-3 styled-checkbox" onchange={e => actions.changeTOC(e.target.checked)} style="width:auto;" />
						<label for="styled-checkbox-1">{ t('acceptance') }</label>
						<a class="summary__modal-btn" onclick={openModal}>
							{ t('termsAndConditions') }
						</a>
						<Section visible={ model.error }>
							<span class="form-error__msg d-block">{ model.error }</span>
						</Section>
					</label>
				</Col>
			</Row>

			<StepButtons
				visible
				next={ t('continue') }
				onclickNext={ actions.submit }>
			</StepButtons>

			<Modal show={ model.showTermAndConditions } onclose={ actions.toggleTermAndConditions } class="w800">
				<div class="termsAndConditions">{ m.trust(model.termsAndConditions) }</div>
				<div class="text-right mt-5 mb-3">
					<a class="button color-6" onclick={ actions.toggleTermAndConditions }>Cerrar</a>
				</div>
			</Modal>

		</div>
	)

}



const Label = tag((attrs, children) => (
	<Row class="mt-4">
		<Col class="text-center">
			<div class="bg-color-2 px-3 py-2 place d-inline-block">
				{ children }
			</div>
		</Col>
	</Row>
))
