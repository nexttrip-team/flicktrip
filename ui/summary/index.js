/*

Render the summary of the service and handle Terms and conditions

*/
const m = require('mithril')
const modal = require('../modal')



/**
 * @typedef {Object} Field
 * @prop {String} label
 * @prop {Array<MithrilVirtualDom>} resume - Example of MithrilVirtualDom: m('h1', 'Hello')
 * 
 * Component attributes
 * @param {Object}       attrs
 * @param {String}       attrs.termAndConditionsURL
 * @param {Function}     attrs.submit
 * @param {Array<Field>} attrs.fields
 * @param {Translator}   attrs.t
 * 
 */
module.exports.oninit = vnode => {

	const t = vnode.attrs.t
	const emitCheckChange = vnode.attrs.oncheck || function(){}

	const model = {
		loading: true, // Loading the terms and condiitons
		tocChecked: false,
		showTermAndConditions: false,
		termsAndConditions: null,
		content: vnode.attrs.content,
		error: null
	}


	const actions = {
		changeTOC: trueOrFalse => { 
			model.tocChecked = trueOrFalse
			model.error = null
		},
		
		submit: () => {
			if( ! model.tocChecked ){
				model.error = t('acceptTosError')
				return
			}
			vnode.attrs.submit()
		},

		toggleTermAndConditions: () => {
			model.showTermAndConditions = !model.showTermAndConditions
		}
	}
	
	if (vnode.attrs.tosURL ){
		fetchTermsAndConditions(vnode.attrs.tosURL)
			.then(contentAsString => {
				model.termsAndConditions = contentAsString
				model.loading = false
				m.redraw()
			})
	} else {
		model.loading = false
	}
	

	vnode.state = { model, actions, t }

}

module.exports.view = require('./view')




function fetchTermsAndConditions(url){
	
	/*
	return new Promise(success => {
		setTimeout(() => {
			success('<h1>Terms and conditions</h1><p>Loaded from services</p>')
		}, 1000)
	})
	*/

	return m.request({ 
		method: 'GET', url,
		headers: {
			//'Access-Control-Allow-Origin': '*',
			'Content-Type': 'text/html; charset=utf-8'
		},
		deserialize: value => value
	})
	.catch(error =>{
		console.error('Fail when try to fecth the terms and conditions', error)
		return ''
	})

	
}