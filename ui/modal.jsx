import m from 'mithril'
import { tag, Icon } from './tags.jsx';
import './modal.css';

function noop() { }

/**
 * @param {Object} attrs
 * @param {Function} attrs.onclose - callback function
 * @param {boolean}  attrs.show - must be visible or not
 */
export default tag((attrs, children) => (
	<div class="modal-window" style={`display: ${attrs.show ? 'block' : 'none'};`}>
		<div class={ `modal-window__content ${ attrs.show ? 'fade-modal' : '' }` } >

			<div class="modal-window__close"
				onclick={
					e => {
						e.preventDefault();
						(attrs.onclose || noop)()
					}
				}>
				<Icon name="x" height="28" width="28" />
			</div>

			<div class="modal-window__body">{children}</div>
		</div>
	</div>
))