const { test } = require('../test.js')
const mq = require('mithril-query')
const Calendar = require('./calendar')
const range = require('ramda/src/range')

const isTrue = a => a === true

const calendar = () => [
	null, null, null, null,
	{ day: 1, state: 'disabled' }, // Jueves, 1 de febrero de 2018
	{ day: 2, free: 6, state: 'selected-range-start' }, // V
	{ day: 3, free: 6, state: 'selected-range' },
	{ day: 4, free: 6, state: 'selected-range' },
	{ day: 5, free: 6, state: 'selected-range-end' }, // L
	{ day: 6, state: 'disabled' },
	{ day: 7, state: 'disabled' },
	{ day: 8, state: 'disabled' },
	{ day: 9,  free: 0, state: 'full-range-start' },
	{ day: 10, free: 0, state: 'full-range' },
	{ day: 11, free: 0, state: 'full-range' },
	{ day: 12, free: 0, state: 'full-range-end' }, // L
	{ day: 13, state: 'disabled' },
	{ day: 14, state: 'disabled' },
	{ day: 15, state: 'disabled' },
	{ day: 16, free: 6, state: null },
	{ day: 17, free: 6, state: null },
	{ day: 18, free: 6, state: null },
	{ day: 19, free: 6, state: null }, // L
	{ day: 20, state: 'disabled' },
	{ day: 21, state: 'disabled' },
	{ day: 22, state: 'disabled' },
	{ day: 23, free: 4, state: null },
	{ day: 24, free: 4, state: null },
	{ day: 25, free: 4, state: null },
	{ day: 26, free: 4, state: null },
	{ day: 27, state: 'disabled' },
	{ day: 28, state: 'disabled' }, // miercoles 28 de febrero
	null, null, null
]

test('Calendar component', t => {

	console.log(`
	Used Febrary of 2018 as example: 
	
	 D  L  M  M  J  V  S
	 x  x  x  x  1  2  3
	 4  5  6  7  8  9 10
	11 12 13 14 15 16 17
	18 19 20 21 22 23 24
	25 26 27 28  x  x  x
	`)

	let clickedData = null 

	const page = mq(Calendar, {
		weekDayNames: [ 'D', 'L', 'M', 'M', 'J', 'V', 'S' ],
		days: calendar(),
		clickOnDay: (data) => { clickedData = data }
	})

	t.ok(
		[ 'D', 'L', 'M', 'M', 'J', 'V', 'S' ].map(dayChar =>
			page.has(`.calendar-header > .calendar-date:contains(${dayChar})`)
		).every(isTrue),
		'Show the row of days ( D M M J V S )'
	)

	const gap = 4

	t.ok(
		range(0, gap).map(i => {
			return page.has(`.calendar-body > .calendar-date:nth-child(${i}) > .d-none`)
		}),
		'Render empty positions of previous month (gap)'
	)

	t.ok(
		[ 1, 6, 7, 8, 13, 14, 15, 20, 21, 22, 27, 28 ].map(i => {
			return page.has(`.calendar-body > .calendar-date:nth-child(${gap + i}) > .strikethrough > button.date-item`)
		}).every(isTrue),
		'Render disabled days'
	)

	const startRangeDay = 2;

	t.ok(
		page.has(`.calendar-body > .calendar-date.calendar-range.range-start:nth-child(${gap + startRangeDay})`),
		'has a start range of selection'
	)

	t.ok(
		[3, 4].map(day => (
			page.has(`.calendar-body > .calendar-date.calendar-range:nth-child(${gap + day})`)
		)).every(isTrue),
		'has 2 day of range selection'
	)

	const endRangeDay = 5;
	t.ok(
		page.has(`.calendar-body > .calendar-date.calendar-range.range-end:nth-child(${gap + endRangeDay})`),
		'has a end range of selection'
	)

	const availableDays = [ 9, 10, 11, 12, 16, 17, 18, 19, 23, 24, 25, 26 ]

	t.ok(
		availableDays.map(day => (
			page.has(`.calendar-body > .calendar-date:nth-child(${gap + day}) > button.date-item`)
		)).every(isTrue),
		'has available days'
	)

	const dayToClick = 9
	t.same(clickedData, null, 'before click, nothing happens')
	page.click(`.calendar-body > .calendar-date:nth-child(${gap + dayToClick}) >  button.date-item`)
	t.same(
		clickedData,
		gap + dayToClick - 1, // Index starts in 0
		'on click a day, the clickOnDay function is called with the index of the calendar'
	)

	const startRangeFullDay = 9
	t.ok(
		page.has(`.calendar-body > .calendar-date.calendar-range.full.range-start:nth-child(${gap + startRangeFullDay})`),
		'has a start range of full period'
	)

	t.ok(
		[ 10, 11 ].map(day => (
			page.has(`.calendar-body > .calendar-date.calendar-range.full:nth-child(${gap + day})`)
		)).every(isTrue),
		'has a list of days full of capacity'
	)

	const endRangeFullDay = 12
	t.ok(
		page.has(`.calendar-body > .calendar-date.calendar-range.full.range-end:nth-child(${gap + endRangeFullDay})`),
		'has a end range full of capacity'
	)

	t.end()

})

