import m from 'mithril'
import './spinner.css'

module.exports = function spinner(/* cssClass */) {
	return (
		//m('.spinner-wrapper', { class: cssClass || "" },
		m('.spinner',
			m('.bounce1'), m('.bounce2'), m('.bounce3'),
			m("h4.text-uppercase.f7.text-center.mt-4", "Estamos procesando la información"),
			m("h4.text-uppercase.f7.text-center.color-2.pb-5.mt-3", "Por favor espere"),
		)
		//)
	)
}
