const m = require('mithril')

const compose = require('ramda/src/compose')
const cond = require('ramda/src/cond')
const isNil = require('ramda/src/isNil')
const assert = require('assert')


/**
 * @function clickOnDay
 * @param {number} calendarIndex
 */

/**
 * @typedef {Object} DayModel
 * @prop {number} day
 * @prop {string} state
 */

/**
 * 
 * @param {object} attrs
 * @param {DayModel[]} attrs.days
 * @param {string[]}   attrs.weekDayNames - List of days (1 character day name). Example: [ 'D', 'L', 'M', 'M', 'J', 'V', 'S' ]
 * @param {clickOnDay} attrs.clickOnDay
 */
module.exports.oninit = ({ attrs, state }) => {
	assert(Array.isArray(attrs.days))
	assert(typeof attrs.clickOnDay == 'function')
	assert(Array.isArray(attrs.weekDayNames))
	assert(attrs.weekDayNames.length == 7)

	state.weekdayNames = attrs.weekDayNames.map(d => m('.calendar-date', d))
}

module.exports.view = ({ attrs, state }) => {

	const { weekdayNames } = state
	const { days, clickOnDay: onclick } = attrs

	try {
		return m('.row.justify-content-center',
			m('.col-12.my-4',
				m('.calendar', 
					m('.calendar-container',
						m('.calendar-header', weekdayNames),
						m('.calendar-body',
							days.map(dayCell(onclick))
						)
					)
				)
			)
		)
	} catch(e) {
		console.error('error', e)
	}
}





const dayBtn = (onclick, cssClass) => ([ day, index ]) => {
	assert(typeof onclick == 'function')
	assert(typeof day == 'number')
	assert(typeof index == 'number')

	return m('button.date-item', { class: cssClass || '', onclick: () => onclick(index) }, day)
}

const activeDayBtn = onclick => ([ day, index ]) => {
	assert(typeof onclick == 'function')
	assert(typeof day == 'number')
	return m('button.date-item.active', { onclick: () => onclick(index) }, day)
}

const fullDayBtn = onclick => ([ day, index ]) => {
	assert(typeof onclick == 'function')
	assert(typeof day == 'number')
	return m('button.date-item.full', { onclick: () => onclick(index) }, day)
}

/**
 * 
 * wrapperDiv :: String -> Vnode -> Vnode
 * 
 * cssClasses: List. Posibles items: 'calendar-range', 'range-start', 'range-end'
*/
const wrapperDiv = cssClasses => children => m('.calendar-date', { class: cssClasses || '' }, children)
const strikethroughDiv = children => m('.strikethrough', children)

const isAvailable = () => true

/**
 * 
 * @param {string} state
 * @param {{ state: string }} data
 * @returns {boolean}
 */
const stateIs = state => data => data.state === state

function noop(){}

// Take day and calendar index
const getDayAndCalendarIndex = ({ day }, index) => [ day, index ]


/**
 * @function dayModelToCell
 * @param {DayModel} dayModel
 * @param {number} calendarIndex
 * @return {Vnode} mithril vnode
 */


/**
 * Map day in calendar to mithril cell day 
 * @param {function} onclick
 * @returns {dayModelToCell} a function to map day to vnode
 */
const dayCell = onclick => cond([
	[ 
		isNil,
		compose(
			wrapperDiv(), 
			() => m('.d-none')
		)
	],[
		stateIs('disabled'),
		compose(
			wrapperDiv(), 
			strikethroughDiv, 
			dayBtn(noop), 
			getDayAndCalendarIndex
		)
	],[
		stateIs('selected-range-start'),
		compose(
			wrapperDiv('calendar-range range-start'),
			dayBtn(onclick, 'active'),
			getDayAndCalendarIndex
		)
	],[
		stateIs('selected-range-end'),
		compose(
			wrapperDiv('calendar-range range-end'),
			dayBtn(onclick, 'active'),
			getDayAndCalendarIndex
		)
	],[
		stateIs('selected-range'),
		compose(
			wrapperDiv('calendar-range'),
			dayBtn(onclick),
			getDayAndCalendarIndex
		)
	],[
		stateIs('full-range-start'),
		compose(
			wrapperDiv('calendar-range range-start full'),
			dayBtn(onclick, 'full'),
			getDayAndCalendarIndex
		)
	],[
		stateIs('full-range'),
		compose(
			wrapperDiv('calendar-range full'),
			dayBtn(onclick),
			getDayAndCalendarIndex
		)
	],[
		stateIs('full-range-end'),
		compose(
			wrapperDiv('calendar-range range-end full'),
			dayBtn(onclick, 'full'),
			getDayAndCalendarIndex
		)
	],[
		stateIs('selected'),
		compose(
			wrapperDiv(),
			activeDayBtn(onclick),
			getDayAndCalendarIndex
		)
	],[
		isAvailable, 
		compose(
			wrapperDiv(),
			dayBtn(onclick),
			getDayAndCalendarIndex
		)
	]
])