import m from 'mithril'
import './select.css'

/**
 * Atributos:
 * + short (boolean): Indica si debe utilizar al versión (visual) corta del componente.
 * + color (string): Color del ícono
 * 
 * Ejemplo de uso:
 * 
 * <Select>
 *	<option value="1">Opción 1</option>
 *	<option value="2">Opción 2</option>
 * </Select>
 * 
 * 
 */

//border-color: var(--font-color-1)

const wrapperClass = attrs => `select-wrapper ${ attrs.short ? 'select-wrapper--short' : '' }`
const wrapperStyle = attrs => `border-color: ${ attrs.color || 'var(--font-color-1)' }`

export default {
	view: ({ attrs, children }) => (
		m('span', { class: wrapperClass(attrs), style: wrapperStyle(attrs) },
			m('select', attrs || {}, children)
		)
	)
} 
