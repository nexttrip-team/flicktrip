require('jsdom-global')()

global.window = Object.assign(
	require('mithril/test-utils/domMock.js')(),
	require('mithril/test-utils/pushStateMock')()
)
const test = require('tape')
process.env.NODE_ENV = 'development'

module.exports = { test }