/*
module.exports = {
    "extends": "standard"
};
*/

module.exports = {
  root: true,
  extends: [
		//"plugin:css-modules/recommended",
		"eslint:recommended"
	],
  env: {
    "browser": true,
    "es6": true,
    "node": true
	},
	rules: {
		"no-console": 0,
		"no-mixed-spaces-and-tabs": [ "error", "smart-tabs" ],
		"react/jsx-uses-vars": 2,
		"no-unused-vars": ["error", { "varsIgnorePattern": "m" }]
	},
	parser: "babel-eslint",
	parserOptions: {
		"ecmaVersion": 6,
		"sourceType": "module",
		"indent": [ "error", "tab" ],
		"ecmaFeatures": { "jsx": true }
	},
	settings: {
		"react": {
			"pragma": "m"
		}
	},
	//plugins: [ "css-modules" ]
	plugins: [ "react" ]
}