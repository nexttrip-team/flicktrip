import { extractErrors, requestWithToken } from './helpers'
import pathOr from 'ramda/src/pathOr'
import convert from 'graphql-obj2arg'

export function excursionPrice(token, url, { seasonId, pax }) {
	const query = `
		mutation {
			calculateExcursionAmount(
				${ convert({ seasonId, paxs: pax }, { noOuterBraces: true })}
			) {
				result {
					amount,
					depositPercentage,
					mandatory
				}
			}
		}
	`

	/*
	response example: 

	{
		"data": {
			"calculateExcursionAmount": {
				"result": {
					"amount": 7600,
					"depositPercentage": 50,
					"mandatory": true
				}
			}
		}
	}
	*/

	return requestWithToken(token, {
		url,
		method: 'POST',
		data: { query }
	}).then(
		pathOr({}, ['calculateExcursionAmount', 'result'])
	)

}