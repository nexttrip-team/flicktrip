import m from 'mithril'
import merge from 'ramda/src/merge'

/**
 * 
 * @param {string} token 
 * @param {object} params 
 * @example of use
 * 
 *	return requestWithToken(token, {
 *		method: 'POST', url: config.apiURL, data: { query }
 *	})
 *	.then(extractData)
 * 
 * 
 */
export function requestWithToken(token, ...params){
	const config = merge({
			background: true,
			headers: {
				"Authorization": `Basic ${token}`
			},
			extract: extractErrors
		},
		...params
	)

	return m.request(config).then(rejectOnError)
}

/**
 * Make an AJAX request with the amount of parameters.
 * @param {Object} params - Request params. See: https://mithril.js.org/request.html
 * 
 */
export function request(params = {}){
	const config = merge({
		background: true,
		extract: extractErrors
	}, params)

	return m.request(config).then(rejectOnError)
}


const rejectOnError = ({ data, error }) => error ? Promise.reject(error) : data

/**
 * 
 * @param {XMLHttpRequest} xhr - @see https://developer.mozilla.org/es/docs/Web/API/XMLHttpRequest
 * @return {({ message: string, status: number } | JSON with status )}
 */
export function extractErrors(xhr) {

	if (xhr.status === 500 || xhr.status === 0) { m.route.set('/500'); return }
	if (xhr.status === 503) { m.route.set('/503'); return }

	if (xhr.status >= 300) {
		const isJSON = xhr.getResponseHeader('content-type').includes('application/json')
		return merge(
			isJSON
				? safeJsonParse(xhr.response)
				: { message: xhr.response },
			{ status: xhr.status }
		)
	}

	return safeJsonParse(xhr.response)
}

const safeJsonParse = str => {
	try {
		return JSON.parse(str)
	} catch (error) {
		console.error('Fail when try to parse: ' + str)
		return {}
	}
}