import { extractErrors } from './helpers'
import { request } from 'mithril'
import path from 'ramda/src/path'

/**
 * @typedef {Object} TokenData
 * @prop {string} tokenHash,
 * @prop {string} rol
 * @prop {boolean} isRegistered
 */

/**
 * Returns a promise with the application token
 * @param {string} url - the URL of the service
 * @param {object} params
 * @param {string} params.provider
 * @param {string} params.accessToken - Login access token (auth0)
 * @return { Promise<Success(TokenData), Reject(error)> }
 */
export function fetchAppToken(url, { customerId, accessToken }) {
	const query = `
		mutation {
			createToken(
				customerId:"${ customerId }",
				accessToken:"${ accessToken }"
			) {
				token {
					cn,
					tokenHash,
					rol,
					isRegistered
				}
			}
		}`

	return request({
		background: true,
		method: 'POST',
		url,
		data: { query },
		extract: extractErrors
	}).then(({ data, error }) => {
		const token = tokenPath(data)
		if( token ){ return token }
		return Promise.reject(error || { message: 'unknown_error' })
	})
}

const tokenPath = path([ 'createToken', 'token' ])